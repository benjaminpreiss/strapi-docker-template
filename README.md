# Strapi Docker database

A repository for strapi CMS run via docker and some helper scripts
This repositry relies on this mysql repository to work properly (since we definitely need a database):

[mysql-docker-template](https://gitlab.com/benjaminpreiss/mysql-docker-template)

## Getting started

- Clone the repository
- Make all scripts executable: `chmod u+x *.sh`
- Make sure that all needed env variables for the bash scripts are defined

## When used as a submodule

### Docker Compose

Copy this into the docker-compose.yml file of the main repo:

```yml
version: '3.1'
services:
# Begin here
    strapi:
        build:
            context: ./strapi
            dockerfile: strapi-dev.Dockerfile
            args: 
                BASE_VERSION: latest
                STRAPI_VERSION: ${STRAPI_DEV_VERSION}
        container_name: ${PROJECT_NAME}-strapi
        platform: linux/amd64
        ports:
            - ${STRAPI_DEV_PORT}:1337
        volumes:
            - ./strapi/volume:/srv/app
        environment:
            DATABASE_CLIENT: 'mysql'
            DATABASE_NAME: ${MYSQL_DEV_DATABASE}
            DATABASE_HOST: ${MYSQL_DEV_CONTAINER_NAME}
            DATABASE_PORT: ${MYSQL_DEV_DB_PORT}
            DATABASE_USERNAME: ${MYSQL_DEV_DB_USER}
            DATABASE_PASSWORD: ${MYSQL_DEV_DB_PASS}
        depends_on:
            - mysql
        networks: 
             - app-network
# End here
networks: 
    app-network:
        name: ${PROJECT_NAME}
```

Since this project needs a database and relies on the repo [mysql-docker-template](https://gitlab.com/benjaminpreiss/mysql-docker-template), make sure that you also have copied the docker compose section from that repo accordingly.

### Env Variables

Make sure that all env variables/secrets are defined inside the main repos config-dev.sh and config-prod.sh. Fill in the values to the following vars:

PROJECT_NAME=...
STRAPI_PROD_VERSION=...

Since this project needs a database and relies on the repo [mysql-docker-template](https://gitlab.com/benjaminpreiss/mysql-docker-template), make sure that you also have set the env vars from that repo accordingly.

...dev:

```bash
#!/bin/bash

# ...

# General

echo PROJECT_NAME='*******' # Your project name here

# Strapi

echo STRAPI_DEV_VERSION=$STRAPI_PROD_VERSION # Your strapi version here
echo STRAPI_DEV_PORT='1337'

# ...

```

...prod:

```bash
#!/bin/bash

# ...

# General

echo STRAPI_PROD_VERSION='4' # Your production strapi version here

# ...

```

## Clearing local strapi data

Run *./clear-strapi-data.sh* to clear the persisted data in ./volume
After that, rerun the docker compose with option --build.
